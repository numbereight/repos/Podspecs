Pod::Spec.new do |spec|

  spec.platform          = :ios
  spec.ios.deployment_target = '9.0'

  spec.name              = 'Insights'
  spec.module_name       = 'Insights'
  spec.version           = '0.4.1'
  spec.summary           = '--'

  spec.homepage          = 'https://www.numbereight.me/'
  spec.documentation_url = 'http://docs.numbereight.me/'
  spec.license           =  { 
    :type => 'MIT',
    :file => 'LICENSE' 
  }
  spec.authors           = { 
    'Oliver Kocsis' => 'oliver@numbereight.me', 
    'Chris Watts' => 'chris@numbereight.me' 
  }

  spec.source = { 
    :git => 'https://gitlab.com/numbereight/insightssdkios', 
    :tag => spec.version 
  }


  spec.cocoapods_version = '>= 1.8.0'
  spec.swift_versions = [ '5.0' ]

  spec.dependency 'NumberEightCore', '>= 0.18.3'
  spec.dependency 'SQLite.swift' , '~> 0.12.2'
  spec.dependency 'Repeat' , '~> 0.5.8'
  spec.dependency 'Codability', '~> 0.2.1'
  spec.source_files = 'Source/*.{swift,h}'

end
