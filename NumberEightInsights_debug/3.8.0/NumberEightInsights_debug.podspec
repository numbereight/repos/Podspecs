Pod::Spec.new do |spec|

  spec.platform          = :ios
  spec.ios.deployment_target = '11.0'

  spec.name              = 'NumberEightInsights_debug'
  spec.module_name       = 'Insights'
  spec.header_dir        = spec.module_name
  spec.version = '3.8.0'
  spec.summary           = '--'

  spec.homepage          = 'https://www.numbereight.ai/'
  spec.documentation_url = 'http://docs.numbereight.ai/'
  spec.license           =  {
    :type => 'MIT',
    :file => 'LICENSE.md'
  }
  spec.authors           = {
    'Matthew Paletta' => 'matt@numbereight.ai',
    'Chris Watts' => 'chris@numbereight.ai'
  }

  spec.source = {
    :git => 'https://gitlab.com/numbereight/repos/nesdkinsightsios-debug-cocoapods',
    :tag => spec.version
  }

  spec.cocoapods_version = '>= 1.8.0'
  spec.swift_versions = [ '5.0' ]

  spec.libraries = "c++"

  spec.dependency 'NumberEightCore_debug', spec.version.to_s
  spec.dependency 'SQLite.swift' , '~> 0.13.0'
  spec.dependency 'Codability', '~> 0.2.1'
  spec.source_files = 'Source/**/*.{h,swift,m,mm,c,cc,cpp}'
  spec.public_header_files = 'Source/**/*.h'
  spec.requires_arc = true
  spec.static_framework = true
end
