Pod::Spec.new do |spec|

  spec.platform          = :ios
  spec.ios.deployment_target = '9.0'

  spec.name              = 'NumberEightInsights_debug'
  spec.module_name       = 'Insights'
  spec.version = '2.6.6'
  spec.summary           = '--'

  spec.homepage          = 'https://www.numbereight.ai/'
  spec.documentation_url = 'http://docs.numbereight.ai/'
  spec.license           =  {
    :type => 'MIT',
    :file => 'LICENSE.md'
  }
  spec.authors           = {
    'Matthew Paletta' => 'matt@numbereight.ai',
    'Chris Watts' => 'chris@numbereight.ai'
  }

  spec.source = {
    :git => 'https://gitlab.com/numbereight/repos/nesdkinsightsios-debug-cocoapods',
    :tag => spec.version
  }

  spec.cocoapods_version = '>= 1.8.0'
  spec.swift_versions = [ '5.0' ]

  spec.dependency 'NumberEightCore_debug', '~> ' + spec.version.to_s
  spec.dependency 'SQLite.swift' , '~> 0.12.2'
  spec.dependency 'Codability', '~> 0.2.1'
  spec.source_files = 'Source/**/*.{h,swift,m,mm,c,cc,cpp}'

end
