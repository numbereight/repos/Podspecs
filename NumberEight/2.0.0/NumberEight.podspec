
Pod::Spec.new do |spec|
  spec.platform = :ios
  spec.ios.deployment_target = '9.0'

  spec.name = 'NumberEight'
  spec.version = '2.0.0'
  spec.license = { :type => 'Proprietary', :file => 'LICENSE' }
  spec.homepage = 'https://www.numbereight.ai/'
  spec.authors = { 'Matthew Paletta' => 'matt@numbereight.ai', 'Chris Watts' => 'chris@numbereight.ai' }
  spec.summary = ' -- '
  spec.source = { :git => 'https://gitlab.com/numbereight/repos/CocoaPodRelease', :tag => spec.version }
  spec.documentation_url = 'http://docs.numbereight.ai/'

  spec.platform          = :ios
  spec.ios.deployment_target = '9.0'
  spec.cocoapods_version = '>= 1.8.0'
  spec.swift_versions = [ '5.0' ]

  spec.default_subspec = 'Core'

  spec.subspec 'Core' do |ss|
    ss.dependency 'NumberEightCore', spec.version.to_s
  end

  spec.subspec 'Insights' do |ss|
    ss.dependency 'Insights', spec.version.to_s
  end

  spec.subspec 'Audiences' do |ss|
    ss.dependency 'Audiences', spec.version.to_s
  end
end
