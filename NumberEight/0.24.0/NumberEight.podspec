
Pod::Spec.new do |spec|
  version = '0.24.0'
  spec.platform = :ios
  spec.ios.deployment_target = '9.0'

  spec.name = 'NumberEight'
  spec.version = version
  spec.license = { :type => 'Proprietary', :file => 'LICENSE' }
  spec.homepage = 'https://www.numbereight.me/'
  spec.authors = { 'Oliver Kocsis' => 'oliver@numbereight.me', 'Chris Watts' => 'chris@numbereight.me' }
  spec.summary = ' -- '
  spec.source = { :git => 'https://gitlab.com/numbereight/nesdkioscocoapod', :tag => version }
  spec.documentation_url = 'http://docs.numbereight.me/'

  spec.platform          = :ios
  spec.ios.deployment_target = '9.0'
  spec.cocoapods_version = '>= 1.8.0'
  spec.swift_versions = [ '5.0' ]
 
  spec.default_subspec = 'Core'

  spec.subspec 'NumberEight' do |ss|
    ss.dependency 'NumberEight/Core', spec.version.to_s
  end

  spec.subspec 'Core' do |ss|
    ss.dependency 'NumberEightCore', spec.version.to_s
  end

  spec.subspec 'Insights' do |ss|
    ss.dependency 'Insights'
  end

end
