
Pod::Spec.new do |spec|
  version = '0.24.2'
  spec.platform = :ios
  spec.ios.deployment_target = '9.0'

  spec.name = 'NumberEight'
  spec.version = version
  spec.license = { :type => 'Proprietary', :file => 'LICENSE' }
  spec.homepage = 'https://www.numbereight.me/'
  spec.authors = { 'Matthew Paletta' => 'matt@numbereight.me', 'Oliver Kocsis' => 'oliver@numbereight.me', 'Chris Watts' => 'chris@numbereight.me' }
  spec.summary = ' -- '
  spec.source = { :git => 'https://gitlab.com/numbereight/repos/CocoaPodRelease', :tag => version }
  spec.documentation_url = 'http://docs.numbereight.me/'

  spec.platform          = :ios
  spec.ios.deployment_target = '9.0'
  spec.cocoapods_version = '>= 1.8.0'
  spec.swift_versions = [ '5.0' ]
 
  spec.default_subspec = 'Core'

  spec.subspec 'Core' do |ss|
    ss.dependency 'NumberEightCore', spec.version.to_s
  end

  spec.subspec 'Insights' do |ss|
    ss.dependency 'Insights'
  end

  spec.subspec 'Audiences' do |ss|
    ss.dependency 'Audiences'
  end

end
