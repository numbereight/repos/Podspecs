Pod::Spec.new do |spec|

  spec.platform = :ios
  spec.ios.deployment_target = '9.0'
  
  spec.name = 'NumberEightCore'
  spec.module_name = 'NumberEight'
  spec.version = '0.24.2'
  spec.license = { :type => 'Proprietary', :file => 'LICENSE' }
  spec.homepage = 'https://www.numbereight.me/'
  spec.authors = { 'Matthew Paletta' => 'matt@numbereight.me', 'Oliver Kocsis' => 'oliver@numbereight.me', 'Chris Watts' => 'chris@numbereight.me' }
  spec.summary = ' -- '
  spec.source = { :git => 'https://gitlab.com/numbereight/repos/CocoaPodRelease', :tag => spec.version }
  spec.frameworks = 'NumberEightCompiled'
  spec.vendored_frameworks = 'NumberEightCompiled.framework'
  spec.documentation_url = 'http://docs.numbereight.me/'
  spec.source_files = 'Source/*.{swift,h}'
  spec.swift_versions = [ '5.0' ]

end
