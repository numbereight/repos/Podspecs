Pod::Spec.new do |spec|

  spec.platform = :ios
  spec.ios.deployment_target = '9.0'

  spec.name = 'NumberEightCore'
  spec.module_name = 'NumberEight'
  spec.version = '2.6.11'
  spec.license = { :type => 'Proprietary', :file => 'LICENSE' }
  spec.homepage = 'https://www.numbereight.ai/'
  spec.authors = { 'Matthew Paletta' => 'matt@numbereight.ai', 'Chris Watts' => 'chris@numbereight.ai' }
  spec.summary = ' -- '
  spec.source = { :git => 'https://gitlab.com/numbereight/repos/CocoaPodRelease', :tag => spec.version }
  spec.documentation_url = 'http://docs.numbereight.ai/'
  spec.source_files = 'Source/**/*.{h,swift,m,mm,c,cc,cpp}'
  spec.swift_versions = [ '5.0' ]

  spec.dependency 'NumberEightCompiled', spec.version.to_s
  spec.requires_arc = true
  spec.static_framework = true
end
