Pod::Spec.new do |spec|

  spec.platform = :ios
  spec.ios.deployment_target = '11.0'

  spec.name = 'NumberEightCoreBeta'
  spec.module_name = 'NumberEight'
  spec.version = '3.11.0'
  spec.license = { :type => 'Proprietary', :file => 'LICENSE' }
  spec.homepage = 'https://www.numbereight.ai/'
  spec.authors = { 'Matthew Paletta' => 'matt@numbereight.ai', 'Chris Watts' => 'chris@numbereight.ai' }
  spec.summary = ' -- '
  spec.source = {
	  :git => 'https://gitlab.com/numbereight/repos/CocoaPodBeta.git',
	  :tag => spec.version
  }
  spec.documentation_url = 'http://docs.numbereight.ai/'
  spec.source_files = 'Source/**/*.{h,swift,m,mm,c,cc,cpp}'
  spec.swift_versions = [ '5.0' ]

  spec.dependency 'NumberEightCompiledBeta', spec.version.to_s
  spec.requires_arc = true
  spec.static_framework = true
end
