Pod::Spec.new do |spec|

  spec.platform          = :ios
  spec.ios.deployment_target = '9.0'

  spec.name              = 'Audiences'
  spec.module_name       = 'Audiences'
  spec.version = '1.4.0'
  spec.summary           = '--'

  spec.homepage          = 'https://www.numbereight.ai/'
  spec.documentation_url = 'http://docs.numbereight.ai/'
  spec.license           =  {
    :type => 'MIT',
    :file => 'LICENSE.md'
  }
  spec.authors           = {
    'Matthew Paletta' => 'matt@numbereight.ai',
    'Chris Watts' => 'chris@numbereight.me'
  }

  spec.source = {
    :git => 'https://gitlab.com/numbereight/community/AudiencesSDKiOS.git',
    :tag => spec.version
  }

  spec.cocoapods_version = '>= 1.8.0'
  spec.swift_versions = [ '5.0' ]

  spec.dependency 'Insights', '~> ' + spec.version.to_s
  spec.source_files = 'Source/**/*.{h,swift,m,mm,c,cc,cpp}'
end
