Pod::Spec.new do |spec|
  spec.platform = :ios
  spec.ios.deployment_target = '9.0'

  spec.name = 'NumberEightCore_debug'
  spec.module_name = 'NumberEight'
  spec.version = '1.1.0'
  spec.license = { :type => 'Proprietary', :file => 'LICENSE' }
  spec.homepage = 'https://numbereight.ai'
  spec.authors = { 'Matthew Paletta' => 'matt@numbereight.ai', 'Chris Watts' => 'chris@numbereight.ai' }
  spec.summary = ' -- '
  spec.source = {
    :git => 'https://gitlab.com/numbereight/repos/CocoaPodDebug',
	:tag => spec.version
  }
  spec.frameworks = 'NumberEightCompiled'
  spec.vendored_frameworks = 'NumberEightCompiled.framework'
  spec.documentation_url = 'http://docs.numbereight.ai'
  spec.source_files = 'Source/*.{swift,h}'
  spec.swift_versions = [ '5.0' ]

  # Temporary Fix for xcode 12
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

end
