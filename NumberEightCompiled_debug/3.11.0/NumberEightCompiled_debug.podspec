Pod::Spec.new do |spec|
  spec.platform = :ios
  spec.ios.deployment_target = '11.0'

  spec.name = 'NumberEightCompiled_debug'
  spec.module_name = 'NumberEightCompiled'
  spec.version = '3.11.0'
  spec.license = { :type => 'Proprietary', :file => 'LICENSE' }
  spec.homepage = 'https://numbereight.ai'
  spec.authors = { 'Matthew Paletta' => 'matt@numbereight.ai', 'Chris Watts' => 'chris@numbereight.ai' }
  spec.summary = ' -- '
  spec.source = {
    :http => "https://storage.googleapis.com/numbereight-debug-sdk/ios/nesdk/3.11.0/NumberEightCompiled.xcframework.zip",
  }
  spec.vendored_frameworks = 'NumberEightCompiled.xcframework'
  spec.documentation_url = 'http://docs.numbereight.ai'
  spec.swift_versions = [ '5.0' ]

end
