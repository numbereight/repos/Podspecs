Pod::Spec.new do |spec|

  spec.platform = :ios
  spec.ios.deployment_target = '11.0'

  spec.name = 'NumberEightCompiledBeta'
  spec.module_name = 'NumberEightCompiled'
  spec.version = '3.11.3'
  spec.license = { :type => 'Proprietary', :file => 'LICENSE' }
  spec.homepage = 'https://www.numbereight.ai/'
  spec.authors = { 'Matthew Paletta' => 'matt@numbereight.ai', 'Chris Watts' => 'chris@numbereight.ai' }
  spec.summary = ' -- '
  spec.source = {
	  :git => 'https://gitlab.com/numbereight/repos/CocoaPodBeta.git',
      :tag => spec.version
  }
  spec.vendored_frameworks = 'NumberEightCompiled.xcframework'
  spec.documentation_url = 'http://docs.numbereight.ai/'
  spec.swift_versions = [ '5.0' ]
end
