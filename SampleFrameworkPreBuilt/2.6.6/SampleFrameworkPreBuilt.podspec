Pod::Spec.new do |spec|
  spec.platform = :ios
  spec.ios.deployment_target = '9.0'

  spec.name = 'SampleFrameworkPreBuilt'
# spec.module_name = 'SampleFramework'
  spec.version = '2.6.6'
  spec.license = {
	  :text => 'Copyright 2021 NumberEight',
	  :type => 'Copyright'
  }
  spec.homepage = 'https://numbereight.ai'
  spec.authors = { 'Matthew Paletta' => 'matt@numbereight.ai', 'Chris Watts' => 'chris@numbereight.ai' }
  spec.summary = ' -- '
  spec.source = {
	  :http => 'https://repo.numbereight.ai/artifactory/generic-local/SampleFramework/v2.6.6./Archive.zip'
#   :git => 'https://gitlab.com/numbereight/repos/sample-cocoapods-integration',
#	:tag => spec.version
  }
  spec.vendored_frameworks = 'Frameworks/SampleFramework.xcframework'
  spec.documentation_url = 'http://docs.numbereight.ai'
  # spec.source_files = 'Source/**/*.{h,swift,m,mm,c,cc,cpp}'
  spec.swift_versions = [ '5.0' ]

# spec.ios.dependency 'NumberEightCompiled', '= 2.6.11'
# spec.static_framework = true
end
